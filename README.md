# LIS4381 Mobile Web Application Development

## Trystan Bannon

### LIS3781 Requirements:

_Course Work Links:_

[A1 README.md](a1/README.md "My a1 README file")

- Install Git
- Create Bitbucket account, make repo
- Connect local to Bitbucket repository
- Install AMPPS
- Install MySQL

[A2 ReadMe.md](a2/README.md "My a2 README file")

- Create database with two tables in mysql
- Insert data into tables, properly connect FK
- Forward engineer to CCI server
- Create 2 users with different grants
- Check grants for both users, make sure they cant do what there not suppose to 

[A3 ReadMe.md](a3/README.md "My a3 README file")

- Connect to the RemoteLabs servers and accsess Oracle
- Create three tables with the proper constraints
- FIll tables with data apropriate to the colum
- Run tables and see if they were filled with your data
- Complete the query reports

[A4 ReadMe.md](a4/README.md "My a4 README file")

- Create in microsoft SQL server a database
- create proper tables with correct relationships
- Populate the tables with correct info
- Complete SQL statement questions

[A5 ReadMe.md](a5/README.md "My a5 README file")

- Add to database made in a4
- Properly add tables to keep with old and new constraints
- Populate tables witch conforming data
- Complete SQL statement questions
  
[P1 ReadMe.md](p1/README.md "My P1 README file")

- Create a working ERD with required tables
- Make sure relationships and buisness rules are meet
- Provide required screenshot of complete ERD
- Provide links to the .mwb and .sql files

[P2 ReadMe.md](p2/README.md "My P2 README file")

- Download and install MongoDB
- Set up .exe and .json files
- Use command line to complete questions for assignment
