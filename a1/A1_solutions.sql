use tdb17c;

select emp_id, emp_fname, emp_lname, 
	CONCAT(emp_street, ", ", emp_city, ", ", emp_state, " ", substring(emp_zip,1,5), '-', substring(emp_zip,6,4)) as address,
    CONCAT('(', substring(emp_phone,1,3), ')',substring(emp_phone,4,3), '-', substring(emp_phone,7,4)) as phone_num,
    CONCAT(substring(emp_ssn,1,3), '-', substring(emp_ssn,4,2), '-', substring(emp_ssn,6,4)) as emp_ssn, job_title
from job as j, employee as e
where j.job_id = e.job_id
order by emp_lname desc;

select e.emp_id, emp_fname, emp_lname, eht_date, eht_job_id, job_title, eht_emp_salary, eht_notes
from employee e, emp_hist h, job j
where e.emp_id = h.emp_id
	and eht_job_id = j.job_id
order by emp_id, eht_date;

SELECT emp_fname, emp_lname, emp_dob,
TIMESTAMPDIFF(year, emp_dob, curdate()) as emp_age,

dep_fname, dep_lname, dep_relation, dep_dob,
TIMESTAMPDIFF(year, dep_dob, curdate()) as dep_age
from employee
	NATURAL JOIN dependent
    order by emp_lname;
    
START TRANSACTION;
	select * from job;
    
    update job
    set job_title = 'owner'
    where job_id = 1;
    
    select * from job;
COMMIT;

DROP PROCEDURE IF EXISTS insert_benefit;
DELIMITER //
CREATE PROCEDURE insert_benefit()
BEGIN
	select * from benefit;
    
    insert into benefit
    (ben_name, ben_notes)
    values
    ('new benefit', 'testing');
    
    select * from benefit;
END //
DELIMITER ;

CALL insert_benefit();
DROP PROCEDURE if EXISTS insert_benefit;

select emp_id, emp_lname, emp_fname, emp_ssn, emp_email,
	dep_lname, dep_fname, dep_ssn, dep_street, emp_city, dep_state, dep_zip, emp_phone
from employee
	natural left outer join dependent
order by emp_lname;

select emp_id,
	concat(emp_lname, ", ", emp_fname) as employee,
    concat(substring(emp_ssn,1,3), '-', substring(emp_ssn,4,2), '-', substring(emp_ssn,6,4)) as emp_ssn,
    emp_email as email,
    
    concat(dep_lname, ", ", dep_fname) as dependent,
    concat(substring(emp_ssn,1,3), '-', substring(dep_ssn,4,2), '-', substring(dep_ssn,6,4)) as dep_ssn,
    concat(dep_street, ", ", emp_city, ", ", dep_state, ", ", substring(dep_zip,1,5), '-', substring(dep_zip,6,4)) as address,
    concat('(', substring(dep_phone,1,3), ')', substring(dep_phone,4,3), '-', substring(dep_phone,7,4)) as phone_num
    
from employee
	natural left outer join dependent
    order by emp_lname;
    
drop trigger if exists trg_employee_after_insert;

delimiter //
create trigger trg_employee_after_insert
	after insert on employee
    for each row
     begin
     insert into emp_hist
		(emp_id, eht_date, eht_type, eht_job_id, eht_emp_salary, eht_usr_changed, eht_reason, eht_notes)
        values
        (new.emp_id, now(), 'i', new-job_id, new.emp_salary, user(), "new employee", new.emp_notes);
        end //
        delimiter ;
        

        

    
    