# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 1 Requirements:

_Five Parts:_

1. Distributed Version Comtrol with Git and Bitbucket
2. AMPPS Instalation
3. Entity Relationship Diagram
4. Chapter Questions (1,2)
5. Bitbucket repo links:
   a) This assignment
   b) completed toutorial (bitbucketstationlocations).

#### README.md file should include the following items:

- Screenshot of A1 ERD
- Ex.1 SQL Soulution
- git commands w/ short description

> #### Git commands w/short descriptions:

1. git init: Initilizes current directory as a git reposotory
2. git status: list what files have been commited or not
3. git add: adds all changes to the next commit
4. git commit: stages the changes to get ready for a push to repo
5. git push: transfers commits and changes to bitbucket
6. git pull: gets intended info from bitbucket to your local repository
7. git log: shows all the commit made for that repository

#### Assignment Screenshots:

_Screenshot of A1 ERD_:

![A1 ERD](Img/A1S.png)

#### Tutorial Links:

_Bitbucket Tutorial - Station Locations:_
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/tdb17c/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
