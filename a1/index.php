<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Trystan D. Bannon">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> This assignment had me create and optimize both a git and Bitbucket account so I could easily push work done on my local device to a repository online for easy acsess elsewhere. 
					Along side this I got my footing with both Java and Android Studio Code. This also included me downloading and running AMPPS for future use with other assignments.
				</p>

				<h4>Java Installation</h4>
				<img src="images/java.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio Installation</h4>
				<img src="images/ASP.png" class="img-responsive center-block" alt="Android Studio Installation" height="500" width="400" >

				<h4>AMPPS Installation</h4>
				<img src="images/ampps1.png" class="img-responsive center-block" alt="AMPPS Installation" height="1000" width="1000">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
