# LIS3781: Advanced Database Management 

## Trystan Bannon

### Assignment 2 Requirements:

_Two Parts:_

1. Create database on mySQL and forward engineer onto cci server
2. Create 2 new users and modify there grants

#### README.md file should include the following items:

- Screenshot of coded database
- Screenshot of both users and there grants

#### Assignment Screenshots:

|                    First half of code                     |                     Second half of code                     |
| :-----------------------------------------------: | :-------------------------------------------------: |
| ![Front page](img/a2_code_1.png){height=100 width=50} | ![second page](img/a2_code_2.png){height=100 width=50} |

|           User 1            |             User 2                 | 
| :-----------------------------: | :-----------------------------------: | 
| ![EvenorOdd](img/user1.png) | ![LargerNumber](img/user2.png) | 
