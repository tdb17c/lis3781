drop database if exists tdb17c;
create database if not exists tdb17c;
use tdb17c;

-------------
-- COMPANY --
-------------

drop table if exists company;
create table if not exists company
(
	cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
    cmp_street VARCHAR(30) NOT NULL,
    cmp_city VARCHAR(30) NOT NULL,
    cmp_state CHAR(2) NOT NULL,
    cmp_zip INT(9) unsigned ZEROFILL NOT NULL COMMENT 'no dashes',
    cmp_phone BIGINT unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
    cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL COMMENT '12,345,678.90',
    cmp_email VARCHAR(100) NULL,
    cmp_url VARCHAR(100) NULL,
    cmp_notes VARCHAR(255) NULL,
    PRIMARY KEY(cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES
(null,'C-Corp','507 - 20th Ave. E. Apt. 2A','Seattle','WA','081226749','2065559857','12345678.00',null,'http://technologies.ci.fsu.edu/node/72','company notes 1'),
(null,'S-Corp','908 W. Capital Way','Tacoma','WA','004011298','2065559482','2345678.99',null,'www.moneyscam.com','company notes 2'),
(null,'Non-Profit-Corp','722 Monn Bay Blvd','Kirkland','WA','000337845','2065553412','134567.89',null,'www.website.com','company notes 3'),
(null,'LLC','4110 Old Redmond Rd.','Redmond','WA','000029021','2065558122','67892.00',null,'www.randomcollection.com','company notes 4'),
(null,'Partnership','4726 - 11th Ave. N.E.','Seattle','WA','001051082','2065551189','321654.00',null,'www.ilikeMCumovies.com','company notes 5');

SHOW WARNINGS;

--------------
-- CUSTOMER --
--------------

DROP TABLE IF EXISTS customer;
create table if not exists customer
(
	cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_id INT UNSIGNED NOT NULL,
    cus_ssn binary(64) NOT NULL,
    cus_PBaJ binary(64) NOT NULL comment'*only* demo purposes - do *NOT* use *salt* in the name!',
    cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
    cus_first VARCHAR(15) NOT NULL,
    cus_last VARCHAR(30) NOT NULL,
    cus_street VARCHAR(30) NULL,
    cus_city VARCHAR(30) NULL,
    cus_state CHAR(2) NULL,
    cus_zip INT(9) unsigned ZEROFILL NULL,
    cus_phone BIGINT unsigned NOT NULL COMMENT'ssn and zip codes can be zero-filled, but not US area codes',
    cus_email VARCHAR(100) NULL,
    cus_balance DECIMAL(7,2) unsigned NULL comment'12,345,678.90',
    cus_tot_sales decimal(7,2) unsigned NULL,
    cus_notes VARCHAR(255) NULL,
    primary key (cus_id),
    
    UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
    INDEX idx_cmp_id (cmp_id ASC),
    
    
    CONSTRAINT fk_customer_company
		FOREIGN KEY (cmp_id)
        REFERENCES company (cmp_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
	
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW warnings;

set @PBaJ = random_bytes(64);

INSERT INTO customer
VALUES
(null,2,unhex(SHA2(CONCAT(@PBaJ, 000456789),512)),@PBaJ,'Discount','Wilbut','Denway','23 Buildings Gate','El Paso','TX','085703412','2145559857','test1@gmail.com','4325.55','54323.66','customer notes 1'),
(null,4,unhex(SHA2(CONCAT(@PBaJ, 001456789),512)),@PBaJ,'Loyal','Bradford','Casis','891 Drift Dr.','Stanton','TX','005819045','2145559482','test2@gmail.com','943.34','43434.44','customer notes 2'),
(null,3,unhex(SHA2(CONCAT(@PBaJ, 002456789),512)),@PBaJ,'Impulse','Valerie','Lieblong','421 Calamari Vista','Odessa','TX','000621134','2145553412','test3@gmail.com','5434.45','56565.45','customer notes 3'),
(null,5,unhex(SHA2(CONCAT(@PBaJ, 003456789),512)),@PBaJ,'Need-Based','Kathy','Jeffries','915 Drive Past','Penwell','TX','009135674','2145558122','test4@gmail.com','456.765','7965.56','customer notes 4'),
(null,1,unhex(SHA2(CONCAT(@PBaJ, 004456789),512)),@PBaJ,'Wandering','Steve','Martin','329 Volume Ave.','Tarzan','TX','000054426','2145551189','test5@gmail.com','3456.65','87654.65','customer notes 5');

SHOW WARNINGS;

select * from company;
select * from customer;