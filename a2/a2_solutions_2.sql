use tdb17c;
show databases;
use mysql;
show tables;

-- select * from db;

CREATE USER IF NOT EXISTS 'user1'@'localhost' IDENTIFIED by 'trystan99';
CREATE USER IF NOT EXISTS 'user2'@'localhost' IDENTIFIED by 'bannon99';
flush privileges;

GRANT SELECT, UPDATE, DELETE
on tdb17c.company
to user1@"localhost";
show warnings;

GRANT SELECT, UPDATE, DELETE
on tdb17c.customer
to user1@"localhost";
show warnings;

GRANT SELECT, INSERT
on tdb17c.customer
TO user2@"localhost";
show warnings;

FLUSH PRIVILEGES;

-- show grants for 'user2'@'localhost';
