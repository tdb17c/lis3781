# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 3 Requirements:

_Parts of the Assignment:_

1. Connect to Oracle server using RemoteLabs
2. Create and fill all three tables
3. Complete reports on tables

#### Assignment Screenshots:

|                        Customer                    |                         Commodity                    | Order |
| :------------------------------------------------: | :--------------------------------------------------: | :---: |
| ![Customer](img/customer.png){height=100 width=50} | ![commodity](img/commodity.png){height=100 width=50} |    ![order](img/order.png){height=100 width=50}   |

|             SQL code pt.1             |          SQL code pt.2          |       SQL code pt.3        | SQL code pt.4  |
| :--------------------------------: | :--------------------------: | :---------------------: | :---: |
| ![pt.1](img/code1.png) | ![pt.2](img/code2.png) | ![pt.3](img/code3.png) |    ![pt.4](img/code4.png)   |


_Code and query reports can be found on the RemoteLabs servers_
