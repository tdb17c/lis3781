# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 4 Requirements:

_Requirements:_

1. Fill database in Microsoft SQLserver with 14 tables
2. Connect tables with proper relationships and constraints 
3. Fill tables with 5-10 rows of data
4. Complete SQL statement questions

#### README.md file should include the following items:

- Screenshots of database diagram
- File of query used for tables, inserts and questions 
#### Assignment Screenshots:

_Database diagram_:

![Carosel Screenshot](img/a4_carosel.png)


|        Part 1         |          Part 2          |          Part 3          |
| :------------------------: | :---------------------------------: | :---------------------------------: |
| ![ArrayList](img/a4-1.png) | ![NestedStructures](img/a4-2.png) | ![NestedStructures](img/a4-3.png) |

_SQL File_:

![sql_file](docs/a4.sql)