set ANSI_WARNINGS ON;
Go
use master;
Go

if exists (select name from master.dbo.sysdatabases where name = N'tdb17c')
drop database tdb17c;

if not exists (select name from master.dbo.sysdatabases where name = N'tdb17c')
create database tdb17c;
go

use tdb17c;
go

if OBJECT_ID (N'dbo.person', N'U') is not null
drop table dbo.person;
go

create table dbo.person
(
	per_id smallint not null identity(1,1),
	per_ssn binary(64) null,
	per_pep binary(64) NULL,
	per_fname varchar(15) not null,
	per_lname varchar(30) not null,
	per_gender char(1) not null check (per_gender IN('m','f')),
	per_dob date not null,
	per_street varchar(30) not null,
	per_city varchar(30) not null,
	per_state char(2) not null default 'FL',
	per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	per_email varchar(100) null,
	per_type char(1) not null check (per_type IN('c','s')),
	per_notes varchar(45) null,
	primary key (per_id),

	constraint ux_per_ssn unique nonclustered (per_ssn ASC)
	);

	if OBJECT_ID (N'dbo.phone', N'U') is not null
	drop table dbo.phone;
	go

	create table dbo.phone
	(
		phn_id smallint not null identity(1,1),
		per_id smallint not null,
		phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
		phn_type char(1) not null check (phn_type IN('h','c','w','f')),
		phn_notes varchar(255) null,
		primary key (phn_id),

		constraint fk_phone_person
		foreign key (per_id)
		references dbo.person (per_id)
		on delete cascade
		on update cascade
	);	

	if OBJECT_ID (N'dbo.customer', N'U') is not null
	drop table dbo.customer;
	go

	create table dbo.customer
	(
		per_id smallint not null,
		cus_balance decimal(7,2) not null check (cus_balance >= 0),
		cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
		cus_notes varchar(45) null,
		primary key (per_id),

		constraint fk_customer_person
		foreign key (per_id)
		references dbo.person (per_id)
		on delete cascade
		on update cascade
	);

	if OBJECT_ID (N'dbo.slsrep', N'U') is not null
	drop table dbo.slsrep;
	go
	create table dbo.slsrep
	(
		per_id smallint not null,
		srp_yr_sales_goal decimal(8,2) not null check (srp_yr_sales_goal >= 0),
		srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
		srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
		srp_notes varchar(45) null,
		primary key (per_id),

		constraint fk_slsrep_person
		foreign key (per_id)
		references dbo.person (per_id)
		on delete cascade
		on update cascade
	);

	if OBJECT_ID (N'dbo.srp.hist', N'U') is not null
	drop table dbo.srp_hist;
	go

	create table dbo.srp_hist
	(
		sht_id smallint not null identity(1,1),
		per_id smallint not null,
		sht_type char(1) not null check (sht_type IN('i','u','d')),
		sht_modified datetime not null,
		sht_modifier varchar(45) not null default system_user,
		sht_date date not null default getDate(),
		sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
		sht_yr_total_sales decimal(8,2) not null check (sht_yr_total_sales >= 0),
		sht_yr_total_comm decimal(7,2) not null check (sht_yr_total_comm >= 0),
		sht_notes varchar(45) null,
		primary key (sht_id),
		
		constraint fk_srp_hist_slsrep
		foreign key (per_id)
		references dbo.slsrep (per_id)
		on delete cascade
		on update cascade
	);

	if OBJECT_ID(N'dbo.contact', N'U') is not null
	drop table dbo.contact;
	Go

	create table dbo.contact
	(
		cnt_id int not null identity(1,1),
		per_cid smallint not null,
		per_sid smallint not null,
		cnt_date datetime not null,
		cnt_notes varchar(255) null,
		primary key (cnt_id),

		constraint fk_contact_customer
		foreign key (per_cid)
		references dbo.customer (per_id)
		on delete cascade
		on update cascade,

		constraint fk_contact_slsrep
		foreign key (per_sid)
		references dbo.slsrep (per_id)
		on delete no action
		on update no action
	);

	if OBJECT_ID (N'dbo.[order]', N'U') is not null
	drop table dbo.[order];
	Go

	create table dbo.[order]
	(
		ord_id int not null identity(1,1),
		cnt_id int not null,
		ord_placed_date datetime not null,
		ord_filled_date datetime null,
		ord_notes varchar(255) null,
		primary key (ord_id),

		constraint fk_order_contact
		foreign key (cnt_id)
		references dbo.contact (cnt_id)
		on delete cascade
		on update cascade
	);

if OBJECT_ID (N'dbo.store', N'U') is not null
drop table dbo.store;
Go

create table dbo.store
(
	str_id smallint not null identity(1,1),
	str_name varchar(45) not null,
	str_street varchar(30) not null,
	str_city varchar(30) not null,
	str_state char(2) not null default 'FL',
	str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	str_email varchar(100) not null,
	str_url varchar(100) not null,
	str_notes varchar(255) null,
	primary key (str_id)
);

if OBJECT_ID (N'dbo.invoice', N'U') is not null
drop table dbo.invoice;
Go

create table dbo.invoice
(
	inv_id int not null identity(1,1),
	ord_id int not null,
	str_id smallint not null,
	inv_date datetime not null,
	inv_total decimal(8,2) not null check (inv_total >= 0),
	inv_paid bit not null,
	inv_notes varchar(255) null,
	primary key (inv_id),

	constraint ux_ord_id unique nonclustered (ord_id ASC),

	constraint fk_invoice_order
	foreign key (ord_id)
	references dbo.[order] (ord_id)
	on delete cascade
	on update cascade,

	constraint fk_invoice_store
	foreign key (str_id)
	references dbo.store (str_id)
	on delete cascade
	on update cascade
);

if OBJECT_ID (N'dbo.payment', N'U') is not null
drop table dbo.payment;
Go

create table dbo.payment
(
	pay_id int not null identity(1,1),
	inv_id int not null,
	pay_date datetime not null,
	pay_amt decimal(7,2) not null check (pay_amt >= 0),
	pay_notes varchar(255) null,
	primary key (pay_id),

	constraint fk_payment_invoice
	foreign key (inv_id)
	references dbo.invoice (inv_id)
	on delete cascade
	on update cascade
);

if OBJECT_ID (N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
Go

create table dbo.vendor
(
	ven_id smallint not null identity(1,1),
	ven_name varchar(45) not null,
	ven_street varchar(30) not null,
	ven_city varchar(30) not null,
	ven_state char(2) not null default 'FL',
	ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	ven_email varchar(100),
	ven_url varchar(100) null,
	ven_notes varchar(255) null,
	primary key (ven_id)
);

if OBJECT_ID (N'dbo.product', N'U') is not null
drop table dbo.product;
Go

create table dbo.product 
(
	pro_id smallint not null identity(1,1),
	ven_id smallint not null,
	pro_name varchar(30) not null,
	pro_descript varchar(45) null,
	pro_weight float not null check (pro_weight >= 0),
	pro_qoh smallint not null check (pro_qoh >= 0),
	pro_cost decimal(7,2) not null check (pro_cost >= 0),
	pro_price decimal(7,2) not null check (pro_price >= 0),
	pro_discount decimal(3,0) null,
	pro_notes varchar(255) null,
	primary key (pro_id),

	constraint fk_product_vendor
	foreign key (ven_id)
	references dbo.vendor (ven_id)
	on delete cascade
	on update cascade
);

if OBJECT_ID (N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
Go

create table dbo.product_hist
(
	pht_id int not null identity(1,1),
	pro_id smallint not null,
	pht_date datetime not null,
	pht_cost decimal(7,2) not null check (pht_cost >= 0),
	pht_price decimal(7,2) not null check (pht_price >= 0),
	pht_discount decimal(3,0) null,
	pht_notes varchar(255) null,
	primary key (pht_id),

	constraint fk_product_hist_product
	foreign key (pro_id)
	references dbo.product (pro_id)
	on delete cascade
	on update cascade
);

if OBJECT_ID (N'dbo.order_line', N'U') is not null
drop table dbo.order_line;
Go

create table dbo.order_line
(
	oln_id int not null identity(1,1),
	ord_id int not null,
	pro_id smallint not null,
	oln_qty smallint not null check (oln_qty >= 0),
	oln_price decimal(7,2) not null check (oln_price >= 0),
	oln_notes varchar(255) null,
	primary key (oln_id),

	constraint fk_order_line_order
	foreign key (ord_id)
	references dbo.[order] (ord_id)
	on delete cascade
	on update cascade,

	constraint fk_order_line_product
	foreign key (pro_id)
	references dbo.product (pro_id)
	on delete cascade
	on update cascade
);

select * from INFORMATION_SCHEMA.TABLES;

insert into dbo.person
(per_ssn,per_pep,per_fname,per_lname,per_gender,per_dob,per_street,per_city,per_state, per_zip, per_email,per_type,per_notes)
values
(1,NULL,'Steve','Rogers','m','1983-03-21','435 Main Dr.','New York','NY',123456789,'srogers@gmail.com','s',NULL),
(2,NULL,'Tony','Stark','m','1999-04-03','456 Second St.','San Diego','CA',234567890,'tstark@gmail.com','s',NULL),
(3,NULL,'Mary','Jane','f','2000-04-20','567 Third Dr.','Jacsonville','FL',345678901,'mjane@gmail.com','s',NULL),
(4,NULL,'Bob','Vance','m','2000-03-05','678 Forth St.','Orlando','FL',456789012,'bvance@gmail.com','s',NULL),
(5,NULL,'Emilia','Clarke','f','1999-11-21','789 Fifth Dr.','Tallahasee','FL',567890123,'eclarke@gmail.com','s',NULL),
(6,NULL,'Bruce','Wayne','m','1990-06-05','890 Sixth Dr.','Gotham','NY',678901234,'bwayne@gmail.com','c',NULL),
(7,NULL,'Clark','Kent','m','1900-02-01','901 Seventh Dr.','New York','NY',789012345,'ckent@gmail.com','c',NULL),
(8,NULL,'Mark','Walberg','m','1980-01-01','123 Eigth Dr.','Portland','OR',890123456,'mwalberg@gmail.com','c',NULL),
(9,NULL,'John','Smith','m','1988-05-06','234 Ninth Dr.','Las Vegas','CA',901234567,'jsmith@gmail.com','c',NULL),
(10,NULL,'Jane','Smith','f','1999-12-30','345 Tenth Dr.','Seattle','WA',987654321,'jasmith@gmail.com','c',NULL);

select * from dbo.person;
--
insert into dbo.phone
(per_id,phn_num,phn_type,phn_notes)
values
(1,1234565432,'h',Null),
(1,2321234565,'c',Null),
(2,3243546576,'w',Null),
(2,6576879865,'f',Null),
(4,5465768798,'c',Null);

select * from dbo.phone;
--
insert into dbo.slsrep
(per_id,srp_yr_sales_goal,srp_ytd_sales,srp_ytd_comm,srp_notes)
values
(1,100000,60000,1800,NULL),
(2,80000,35000,3500,NULL),
(3,150000,84000,9650,NULL),
(4,125000,87000,15300,NULL),
(5,98000,43000,8750,NULL);

select * from dbo.slsrep;
--
insert into dbo.customer
(per_id,cus_balance,cus_total_sales,cus_notes)
values
(6,120,14789,NULL),
(7,98.46,234.92,NULL),
(8,0,4578,'Customer always pays on time.'),
(9,981.73,1672.38,'High balance'),
(10,541.23,782.57,NULL);

select * from dbo.customer;
--
insert into dbo.contact
(per_sid,per_cid,cnt_date,cnt_notes)
values
(1,6,1999-01-01,NULL),
(2,7,2001-09-29,NULL),
(3,8,2002-08-15,NULL),
(4,9,2002-09-01,NULL),
(5,10,2004-01-05,NULL);

select * from dbo.contact;
--
insert into dbo.[order]
(cnt_id,ord_placed_date,ord_filled_date,ord_notes)
values
(1,'2010-11-23','2010-12-24',NULL),
(2,'2005-03-19','2005-07-28',NULL),
(3,'2011-07-01','2011-07-06',NULL),
(4,'2009-12-24','2010-01-05',NULL),
(5,'2008-09-21','2008-11-26',NULL);

select * from dbo.[order];
--
insert into dbo.store
(str_name,str_street,str_city,str_state,str_zip,str_phone,str_email,str_url,str_notes)
values
('Walgreens','14567 Walnut Ln','Aspen','IL','475315690','3127658127','info@walgreens.com','www.walgreens.com',NULL),
('CVS','572 Casper Rd','Chicago','IL','505231519','3128926534','help@cvs.com','ww.cvs.com','Rumor of merger'),
('Lowes','81309 Catapult Ave','Clover','WA','802345671','9017654321','sales@lowes.com','www.lowes.com',NULL),
('Walmart','14567 Walnut Ln','St. Louis','FL','387563628','8722718923','info@walmart.com','www.walmart.com',NULL),
('Dollar General','47583 Davison Rd','Detroit','MI','482983456','3137583492','ask@dollartree.com','www.dollargeneral.com',NULL);

select * from dbo.store;
--
insert into dbo.invoice
(ord_id,str_id,inv_date,inv_total,inv_paid,inv_notes)
values
(5,1,'2001-05-03',58.32,0,Null),
(4,1,'2006-11-11',100.59,0,Null),
(1,1,'2010-09-16',57.34,0,Null),
(3,2,'2011-01-10',99.32,1,Null),
(2,3,'2008-06-24',1109.67,1,Null);

select * from dbo.invoice;
--
insert into dbo.vendor
(ven_name,ven_street,ven_city,ven_state,ven_zip,ven_phone,ven_email,ven_url,ven_notes)
values
('Syco','531 Dolphin Run','Orlando','FL','344761234','7641238543','sales@sysco.com','ww.sysco.com',NULL),
('General Eletric','100 Happy Trails Dr','Boston','MA','123458743','2134569641','support@ge.com','www.ge.com',NULL),
('Cisco','300 Cisco Dr','Stanford','OR','782315492','7823456723','cisco@cisco.com','www.cisco.com',NULL),
('Goodyear','100 Goodyear Dr','Gary','IN','485321956','5784218427','sales@goodyear.com','www.goodyear.com',NULL),
('Snap-on','42185 Magenta Ave','Lake Falls','ND','387513649','9197345632','support@snapon.com','www.snap-on.com','Good quality tools');

select * from dbo.vendor;
--
insert into dbo.product
(ven_id,pro_name,pro_descript,pro_weight,pro_qoh,pro_cost,pro_price,pro_discount,pro_notes)
values
(1,'hammer','',2.5,45,4.99,7.99,30,'Discounted only when purchased with screwdriver set.'),
(2,'screwdriver','',1.8,120,1.99,3.49,Null,NULL),
(3,'pail','16 Gallon',2.8,48,3.89,7.99,40,NULL),
(4,'cooking oil','peanut oil',15,19,19.99,28.99,NULL,'gallons'),
(5,'frying pan','',3.5,178,8.45,13.99,50,'Currently 1/2 price sale');

select * from dbo.product;
--
insert into dbo.order_line
(ord_id,pro_id,oln_qty,oln_price,oln_notes)
values
(1,2,10,8.0,NULL),
(2,3,7,9.88,NULL),
(3,4,3,6.99,NULL),
(5,1,2,12.76,NULL),
(4,5,13,58.99,NULL);

select * from dbo.order_line;
--
insert into dbo.payment
(inv_id,pay_date,pay_amt,pay_notes)
values
(5,'2008-07-01',5.99,NULL),
(4,'2010-09-28',4.99,NULL),
(1,'2008-07-23',8.75,NULL),
(3,'2010-10-31',19.55,NULL),
(2,'2011-03-29',32.5,NULL);

select * from dbo.payment;
--
insert into dbo.product_hist
(pro_id,pht_date,pht_cost,pht_price,pht_discount,pht_notes)
values
(1,'2005-01-02 11:53:34',4.99,7.99,30,'Discounted when purchased with screwdriver set'),
(2,'2005-02-03 09:13:56',1.99,3.49,NULL,null),
(3,'2005-03-04 23:21:49',3.89,7.99,40,NULL),
(4,'2006-05-06 18:09:04',19.99,28.99,NULL,'gallons'),
(5,'2006-05-07 15:07:29',8.45,13.99,50,'Currently 1/2 price sale');

select * from dbo.product_hist;
--
insert into dbo.srp_hist
(per_id,sht_type,sht_modified,sht_modifier,sht_date,sht_yr_sales_goal,sht_yr_total_sales,sht_yr_total_comm,sht_notes)
values
(1,'i',getDate(),system_user,getDate(),100000,110000,11000,NULL),
(2,'i',getDate(),system_user,getDate(),150000,175000,17500,NULL),
(3,'u',getDate(),system_user,getDate(),200000,185000,18500,NULL),
(4,'u',getDate(),original_login(),getDate(),210000,220000,22000,NULL),
(5,'i',getDate(),original_login(),getDate(),225000,230000,2300,NULL);

select * from dbo.srp_hist;
--


--REPORTS--
--1--
if OBJECT_ID (N'dbo.v_paid_invoice_total', N'V') is not null
drop view dbo.v_paid_invoice_total;
go

create view dbo.v_paid_invoice_total as
select p.per_id,per_fname,per_lname,sum(inv_total) as sum_total, FORMAT(sum(inv_total),'C', 'en-us') as paid_invoice_total
from dbo.person p
join dbo.customer c on p.per_id=c.per_id
join dbo.contact ct on c.per_id=ct.per_cid
join dbo.[order] o on ct.cnt_id=o.cnt_id
join dbo.invoice i on o.ord_id=i.ord_id
where inv_paid !=0
group by p.per_id,per_fname,per_lname
go

select per_id,per_fname,per_lname,paid_invoice_total from dbo.v_paid_invoice_total order by sum_total desc;
go

--2--
if OBJECT_ID(N'dbo.sp_all_customers_outstanding_balances', N'P') is not null
drop proc dbo.sp_all_customers_outstanding_balances
go

create proc dbo.sp_all_customers_outstanding_balaces as
begin
select p.per_id, per_fname, per_lname,
sum(pay_amt) as total_paid, (inv_total - sum(pay_amt)) invoice_diff
from person p
join dbo.customer c on p.per_id=c.per_id
join dbo.contact ct on c.per_id=ct.per_cid
join dbo.[order] o on  ct.cnt_id=o.cnt_id
join dbo.invoice i on o.ord_id=i.ord_id
join dbo.payment pt on i.inv_id=pt.inv_id
group by p.per_id,per_fname,per_lname,inv_total
order by invoice_diff desc;
end
go

exec dbo.sp_all_customers_outstanding_balaces;

--3--
if OBJECT_ID(N'dbo.sp_populate_srp_hist_table',N'P') is not null
drop proc dbo.sp_populate_srp_hist_table
go

create proc dbo.sp_populate_srp_hist_table as
begin
insert into dbo.srp_hist
(per_id,sht_type,sht_modified,sht_modifier,sht_date,sht_yr_sales_goal,sht_yr_total_sales,sht_yr_total_comm,sht_notes)
select per_id,'i',getDate(),SYSTEM_USER,getDate(),srp_yr_sales_goal,srp_ytd_sales,srp_ytd_comm,srp_notes
from dbo.slsrep;
end
go

--4--
if OBJECT_ID(N'dbo.trg_sales_history_insert', N'TR') is not null
drop trigger dbo.trg_sales_history_insert
go

create trigger dbo.trg_sales_history_insert
on dbo.slsrep
after insert as begin

declare
@per_id_v smallint,
@sht_type_v char(1),
@sht_modified_v date,
@sht_modifier_v varchar(45),
@sht_date_v date,
@sht_yr_sales_goal_v decimal(8,2),
@sht_yr_total_sales_v decimal(8,2),
@sht_yr_total_comm_v decimal(7,2),
@sht_notes_v varchar(255);

select
@per_id_v=per_id,
@sht_type_v='i',
@sht_modified_v=getDate(),
@sht_modifier_v=SYSTEM_USER,
@sht_date_v=getDate(),
@sht_yr_sales_goal_v=srp_yr_sales_goal,
@sht_yr_total_sales_v=srp_ytd_sales,
@sht_yr_total_comm_v=srp_ytd_comm,
@sht_notes_v=srp_notes
from inserted;

insert into dbo.srp_hist
(per_id,sht_type,sht_modified,sht_modifier,sht_date,sht_yr_sales_goal,sht_yr_total_sales,sht_yr_total_comm,sht_notes)
values
(@per_id_v,@sht_type_v,@sht_modified_v,@sht_modifier_v,@sht_date_v,@sht_yr_sales_goal_v,@sht_yr_total_sales_v,@sht_yr_total_comm_v,@sht_notes_v);
end
go

--5--
if OBJECT_ID(N'dbo.trg_product_history_insert', N'TR') is not null
drop trigger dbo.trg_product_history_insert
go

create trigger dbo.trg_product_history_insert
on dbo.product
after insert as 
begin
declare
@pro_id_v smallint,
@pht_modified_v date,
@pht_cost_v decimal(7,2),
@pht_price_v decimal(7,2),
@pht_discount_v decimal(3,0),
@pht_notes_v varchar(255);

select
@pro_id_v=pro_id,
@pht_modified_v=getDate(),
@pht_cost_v=pro_cost,
@pht_price_v=pro_price,
@pht_discount_v=pro_discount,
@pht_notes_v=pro_notes
from inserted;

insert into dbo.product_hist
(pro_id,pht_date,pht_cost,pht_price,pht_discount,pht_notes)
values
(@pro_id_v,@pht_modified_v,@pht_cost_v,@pht_price_v,@pht_discount_v,@pht_notes_v);
end
go

--Extra Credit--
if OBJECT_ID(N'dbo.sp_annual_salesrep_sales_goal', N'P') is not null
drop proc dbo.sp_annual_salesrep_sales_goal
go

create proc dbo.sp_annual_salesrep_sales_goal as
begin
update slsrep
set srp_yr_sales_goal=sht_yr_total_sales * 1.08
from slsrep as sr
join srp_hist as sh
on sr.per_id=sh.per_id
where sht_date=(select max(sht_date) from srp_hist);
end
go