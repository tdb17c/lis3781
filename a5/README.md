 LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 5 Requirements:

_Requirements:_

1. Add an additional 5 tables to the database made in a4
2. Connect tables with proper relationships and constraints 
3. Fill tables with 5-25 rows of data
4. Complete SQL statement questions (with extra credit)

#### README.md file should include the following items:

- Screenshots of database diagram
- File of query used for tables, inserts and questions
#### Assignment Screenshots:

_Database diagram_:

![NestedStructures](img/a5.png)

_SQL File_:

![sql_file](docs/a5.sql)