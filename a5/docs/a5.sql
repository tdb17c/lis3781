set ANSI_WARNINGS ON;
Go
use master;
Go

if exists (select name from master.dbo.sysdatabases where name = N'tdb17c')
drop database tdb17c;

if not exists (select name from master.dbo.sysdatabases where name = N'tdb17c')
create database tdb17c;
go

use tdb17c;
go

if OBJECT_ID (N'dbo.person', N'U') is not null
drop table dbo.person;
go

create table dbo.person
(
	per_id smallint not null identity(1,1),
	per_ssn binary(64) null,
	per_pep binary(64) NULL,
	per_fname varchar(15) not null,
	per_lname varchar(30) not null,
	per_gender char(1) not null check (per_gender IN('m','f')),
	per_dob date not null,
	per_street varchar(30) not null,
	per_city varchar(30) not null,
	per_state char(2) not null default 'FL',
	per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	per_email varchar(100) null,
	per_type char(1) not null check (per_type IN('c','s')),
	per_notes varchar(45) null,
	primary key (per_id),

	constraint ux_per_ssn unique nonclustered (per_ssn ASC)
	);


	if OBJECT_ID (N'dbo.phone', N'U') is not null
	drop table dbo.phone;
	go

	create table dbo.phone
	(
		phn_id smallint not null identity(1,1),
		per_id smallint not null,
		phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
		phn_type char(1) not null check (phn_type IN('h','c','w','f')),
		phn_notes varchar(255) null,
		primary key (phn_id),

		constraint fk_phone_person
		foreign key (per_id)
		references dbo.person (per_id)
		on delete cascade
		on update cascade
	);	


	if OBJECT_ID (N'dbo.customer', N'U') is not null
	drop table dbo.customer;
	go

	create table dbo.customer
	(
		per_id smallint not null,
		cus_balance decimal(7,2) not null check (cus_balance >= 0),
		cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
		cus_notes varchar(45) null,
		primary key (per_id),

		constraint fk_customer_person
		foreign key (per_id)
		references dbo.person (per_id)
		on delete cascade
		on update cascade
	);


	if OBJECT_ID (N'dbo.slsrep', N'U') is not null
	drop table dbo.slsrep;
	go
	create table dbo.slsrep
	(
		per_id smallint not null,
		srp_yr_sales_goal decimal(8,2) not null check (srp_yr_sales_goal >= 0),
		srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
		srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
		srp_notes varchar(45) null,
		primary key (per_id),

		constraint fk_slsrep_person
		foreign key (per_id)
		references dbo.person (per_id)
		on delete cascade
		on update cascade
	);


	if OBJECT_ID (N'dbo.srp.hist', N'U') is not null
	drop table dbo.srp_hist;
	go

	create table dbo.srp_hist
	(
		sht_id smallint not null identity(1,1),
		per_id smallint not null,
		sht_type char(1) not null check (sht_type IN('i','u','d')),
		sht_modified datetime not null,
		sht_modifier varchar(45) not null default system_user,
		sht_date date not null default getDate(),
		sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
		sht_yr_total_sales decimal(8,2) not null check (sht_yr_total_sales >= 0),
		sht_yr_total_comm decimal(7,2) not null check (sht_yr_total_comm >= 0),
		sht_notes varchar(45) null,
		primary key (sht_id),
		
		constraint fk_srp_hist_slsrep
		foreign key (per_id)
		references dbo.slsrep (per_id)
		on delete cascade
		on update cascade
	);


	if OBJECT_ID(N'dbo.contact', N'U') is not null
	drop table dbo.contact;
	Go

	create table dbo.contact
	(
		cnt_id int not null identity(1,1),
		per_cid smallint not null,
		per_sid smallint not null,
		cnt_date datetime not null,
		cnt_notes varchar(255) null,
		primary key (cnt_id),

		constraint fk_contact_customer
		foreign key (per_cid)
		references dbo.customer (per_id)
		on delete cascade
		on update cascade,

		constraint fk_contact_slsrep
		foreign key (per_sid)
		references dbo.slsrep (per_id)
		on delete no action
		on update no action
	);


	if OBJECT_ID (N'dbo.[order]', N'U') is not null
	drop table dbo.[order];
	Go

	create table dbo.[order]
	(
		ord_id int not null identity(1,1),
		cnt_id int not null,
		ord_placed_date datetime not null,
		ord_filled_date datetime null,
		ord_notes varchar(255) null,
		primary key (ord_id),

		constraint fk_order_contact
		foreign key (cnt_id)
		references dbo.contact (cnt_id)
		on delete cascade
		on update cascade
	);

if object_id (N'dbo.region', N'U') is not NULL
drop table dbo.region;
go

create table region
(
  reg_id tinyint not null IDENTITY(1,1),
  reg_name char(1) not null,
  reg_notes varchar(255) null,
  primary key (reg_id)
);
GO

if object_id (N'dbo.state', N'U') is not NULL
drop table dbo.state;
Go 

create table dbo.state
(
  ste_id tinyint not null identity(1,1),
  reg_id tinyint not null,
  ste_name char(2) not null DEFAULT 'FL',
  ste_notes varchar(255) null,
  primary key (ste_id),

  constraint fk_state_region
  foreign key (reg_id)
  REFERENCES dbo.region (reg_id)
  on DELETE CASCADE
  on update cascade
);
GO

if object_id (N'dbo.city', N'U') is not NULL
drop table dbo.city;
GO

create table dbo.city
(
  cty_id smallint not null IDENTITY(1,1),
  ste_id tinyint not null,
  cty_name varchar(30) not null,
  cty_notes varchar(255) null,
  primary key (cty_id),

  constraint fk_city_state
  foreign key (ste_id)
  REFERENCES dbo.state (ste_id)
  on DELETE CASCADE
  on UPDATE CASCADE
);
go 

if OBJECT_ID (N'dbo.store', N'U') is not null
drop table dbo.store;
Go

create table dbo.store
(
  str_id smallint not null identity(1,1),
  cty_id smallint not null,
  str_name varchar(45) not null,
  str_street varchar(30) not null,
  str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  str_email varchar(100) not null,
  str_url varchar(100) not null,
  str_notes varchar(255) null,
  primary key (str_id),

  constraint fk_store_city
  foreign key (cty_id)
  references dbo.city (cty_id)
  on DELETE CASCADE
  on UPDATE CASCADE
);
GO

if OBJECT_ID (N'dbo.invoice', N'U') is not null
drop table dbo.invoice;
Go

create table dbo.invoice
(
	inv_id int not null identity(1,1),
	ord_id int not null,
	str_id smallint not null,
	inv_date datetime not null,
	inv_total decimal(8,2) not null check (inv_total >= 0),
	inv_paid bit not null,
	inv_notes varchar(255) null,
	primary key (inv_id),

	constraint ux_ord_id unique nonclustered (ord_id ASC),

	constraint fk_invoice_order
	foreign key (ord_id)
	references dbo.[order] (ord_id)
	on delete cascade
	on update cascade,

	constraint fk_invoice_store
	foreign key (str_id)
	references dbo.store (str_id)
	on delete cascade
	on update cascade
);

if OBJECT_ID (N'dbo.payment', N'U') is not null
drop table dbo.payment;
Go

create table dbo.payment
(
	pay_id int not null identity(1,1),
	inv_id int not null,
	pay_date datetime not null,
	pay_amt decimal(7,2) not null check (pay_amt >= 0),
	pay_notes varchar(255) null,
	primary key (pay_id),

	constraint fk_payment_invoice
	foreign key (inv_id)
	references dbo.invoice (inv_id)
	on delete cascade
	on update cascade
);

if OBJECT_ID (N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
Go

create table dbo.vendor
(
	ven_id smallint not null identity(1,1),
	ven_name varchar(45) not null,
	ven_street varchar(30) not null,
	ven_city varchar(30) not null,
	ven_state char(2) not null default 'FL',
	ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	ven_email varchar(100),
	ven_url varchar(100) null,
	ven_notes varchar(255) null,
	primary key (ven_id)
);

if OBJECT_ID (N'dbo.product', N'U') is not null
drop table dbo.product;
Go

create table dbo.product 
(
	pro_id smallint not null identity(1,1),
	ven_id smallint not null,
	pro_name varchar(30) not null,
	pro_descript varchar(45) null,
	pro_weight float not null check (pro_weight >= 0),
	pro_qoh smallint not null check (pro_qoh >= 0),
	pro_cost decimal(7,2) not null check (pro_cost >= 0),
	pro_price decimal(7,2) not null check (pro_price >= 0),
	pro_discount decimal(3,0) null,
	pro_notes varchar(255) null,
	primary key (pro_id),

	constraint fk_product_vendor
	foreign key (ven_id)
	references dbo.vendor (ven_id)
	on delete cascade
	on update cascade
);

if OBJECT_ID (N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
Go

create table dbo.product_hist
(
	pht_id int not null identity(1,1),
	pro_id smallint not null,
	pht_date datetime not null,
	pht_cost decimal(7,2) not null check (pht_cost >= 0),
	pht_price decimal(7,2) not null check (pht_price >= 0),
	pht_discount decimal(3,0) null,
	pht_notes varchar(255) null,
	primary key (pht_id),

	constraint fk_product_hist_product
	foreign key (pro_id)
	references dbo.product (pro_id)
	on delete cascade
	on update cascade
);

if OBJECT_ID (N'dbo.order_line', N'U') is not null
drop table dbo.order_line;
Go

create table dbo.order_line
(
	oln_id int not null identity(1,1),
	ord_id int not null,
	pro_id smallint not null,
	oln_qty smallint not null check (oln_qty >= 0),
	oln_price decimal(7,2) not null check (oln_price >= 0),
	oln_notes varchar(255) null,
	primary key (oln_id),

	constraint fk_order_line_order
	foreign key (ord_id)
	references dbo.[order] (ord_id)
	on delete cascade
	on update cascade,

	constraint fk_order_line_product
	foreign key (pro_id)
	references dbo.product (pro_id)
	on delete cascade
	on update cascade
);

if OBJECT_ID (N'dbo.time', N'U') is not NULL
drop TABLE dbo.time;
GO

create table dbo.time
(
  tim_id int not null identity(1,1),
  tim_yr SMALLINT not null, --2 byte integer (no year date type in ms sql server)
  tim_qtr tinyint not null,
  tim_month tinyint not null,
  tim_week tinyint not null,
  tim_day tinyint not null,
  tim_time time not null,
  tim_notes VARCHAR(255) null,
  primary key (tim_id)
);
GO

if object_id (N'dbo.sale', N'U') is not NULL
drop table dbo.sale;
GO 

create TABLE dbo.sale
(
  pro_id smallint not null,
  str_id smallint not null,
  cnt_id int not null,
  tim_id int not null,
  sal_qty smallint not null,
  sal_price decimal(8,2) not null,
  sal_total decimal(8,2) not null,
  sal_notes varchar(255) null,
  primary key (pro_id, cnt_id, tim_id, str_id),

  constraint ux_pro_id_str_id_cnt_id_tim_id
  unique nonclustered (pro_id asc, str_id asc, cnt_id asc, tim_id ASC),

  constraint fk_sale_time
  foreign key (tim_id)
  references dbo.time (tim_id)
  on DELETE CASCADE
  on UPDATE CASCADE,

  constraint fk_sale_contact
  foreign key (cnt_id)
  REFERENCES dbo.contact (cnt_id)
  on DELETE CASCADE
  on UPDATE CASCADE,

  constraint fk_sale_store
  foreign key (str_id)
  REFERENCES dbo.store (str_id)
  on DELETE CASCADE
  on UPDATE CASCADE,

  constraint fk_sale_product 
  foreign key (pro_id)
  REFERENCES dbo.product (pro_id)
  on DELETE CASCADE
  on UPDATE CASCADE
);
GO



select * from INFORMATION_SCHEMA.TABLES;

insert into dbo.person
(per_ssn,per_pep,per_fname,per_lname,per_gender,per_dob,per_street,per_city,per_state, per_zip, per_email,per_type,per_notes)
values
(1,NULL,'Steve','Rogers','m','1983-03-21','435 Main Dr.','New York','NY',123456789,'srogers@gmail.com','s',NULL),
(2,NULL,'Tony','Stark','m','1999-04-03','456 Second St.','San Diego','CA',234567890,'tstark@gmail.com','s',NULL),
(3,NULL,'Mary','Jane','f','2000-04-20','567 Third Dr.','Jacsonville','FL',345678901,'mjane@gmail.com','s',NULL),
(4,NULL,'Bob','Vance','m','2000-03-05','678 Forth St.','Orlando','FL',456789012,'bvance@gmail.com','s',NULL),
(5,NULL,'Emilia','Clarke','f','1999-11-21','789 Fifth Dr.','Tallahasee','FL',567890123,'eclarke@gmail.com','s',NULL),
(6,NULL,'Bruce','Wayne','m','1990-06-05','890 Sixth Dr.','Gotham','NY',678901234,'bwayne@gmail.com','c',NULL),
(7,NULL,'Clark','Kent','m','1900-02-01','901 Seventh Dr.','New York','NY',789012345,'ckent@gmail.com','c',NULL),
(8,NULL,'Mark','Walberg','m','1980-01-01','123 Eigth Dr.','Portland','OR',890123456,'mwalberg@gmail.com','c',NULL),
(9,NULL,'John','Smith','m','1988-05-06','234 Ninth Dr.','Las Vegas','CA',901234567,'jsmith@gmail.com','c',NULL),
(10,NULL,'Jane','Smith','f','1999-12-30','345 Tenth Dr.','Seattle','WA',987654321,'jasmith@gmail.com','c',NULL);

select * from dbo.person;
--
insert into dbo.phone
(per_id,phn_num,phn_type,phn_notes)
values
(1,1234565432,'h',Null),
(1,2321234565,'c',Null),
(2,3243546576,'w',Null),
(2,6576879865,'f',Null),
(4,5465768798,'c',Null);

select * from dbo.phone;
--
insert into dbo.slsrep
(per_id,srp_yr_sales_goal,srp_ytd_sales,srp_ytd_comm,srp_notes)
values
(1,100000,60000,1800,NULL),
(2,80000,35000,3500,NULL),
(3,150000,84000,9650,NULL),
(4,125000,87000,15300,NULL),
(5,98000,43000,8750,NULL);

select * from dbo.slsrep;
--
insert into dbo.customer
(per_id,cus_balance,cus_total_sales,cus_notes)
values
(6,120,14789,NULL),
(7,98.46,234.92,NULL),
(8,0,4578,'Customer always pays on time.'),
(9,981.73,1672.38,'High balance'),
(10,541.23,782.57,NULL);

select * from dbo.customer;
--
insert into dbo.contact
(per_sid,per_cid,cnt_date,cnt_notes)
values
(1,6,1999-01-01,NULL),
(2,7,2001-09-29,NULL),
(3,8,2002-08-15,NULL),
(4,9,2002-09-01,NULL),
(5,10,2004-01-05,NULL);

select * from dbo.contact;
--
insert into dbo.[order]
(cnt_id,ord_placed_date,ord_filled_date,ord_notes)
values
(1,'2010-11-23','2010-12-24',NULL),
(2,'2005-03-19','2005-07-28',NULL),
(3,'2011-07-01','2011-07-06',NULL),
(4,'2009-12-24','2010-01-05',NULL),
(5,'2008-09-21','2008-11-26',NULL);

select * from dbo.[order];
--
insert into dbo.region
(reg_name, reg_notes)
VALUES
('c', NULL),
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL);
GO

select * from dbo.region;

insert into dbo.state
(reg_id, ste_name, ste_notes)
VALUES
(1,'MI',NULL),
(3,'Il',NULL),
(4,'WA',NULL),
(5,'FL',NULL),
(2,'TX',NULL);
GO

select * from dbo.state;

insert into dbo.city
(ste_id,cty_name, cty_notes)
VALUES
(1,'Detroit',NULL),
(2,'Aspen',NULL),
(2,'Chicago',NULL),
(3,'Clover',NULL),
(4,'St. Louis',NULL);
GO

select * from dbo.city;
--

insert into dbo.store
(cty_id,str_name,str_street,str_zip,str_phone,str_email,str_url,str_notes)
values
(2,'Walgreens','14567 Walnut Ln','475315690','3127658127','info@walgreens.com','www.walgreens.com',NULL),
(3,'CVS','572 Casper Rd','505231519','3128926534','help@cvs.com','ww.cvs.com','Rumor of merger'),
(4,'Lowes','81309 Catapult Ave','802345671','9017654321','sales@lowes.com','www.lowes.com',NULL),
(5,'Walmart','14567 Walnut Ln','387563628','8722718923','info@walmart.com','www.walmart.com',NULL),
(1,'Dollar General','47583 Davison Rd','482983456','3137583492','ask@dollartree.com','www.dollargeneral.com',NULL);

select * from dbo.store;
--
insert into dbo.invoice
(ord_id,str_id,inv_date,inv_total,inv_paid,inv_notes)
values
(5,1,'2001-05-03',58.32,0,Null),
(4,1,'2006-11-11',100.59,0,Null),
(1,1,'2010-09-16',57.34,0,Null),
(3,2,'2011-01-10',99.32,1,Null),
(2,3,'2008-06-24',1109.67,1,Null);

select * from dbo.invoice;
--
insert into dbo.vendor
(ven_name,ven_street,ven_city,ven_state,ven_zip,ven_phone,ven_email,ven_url,ven_notes)
values
('Syco','531 Dolphin Run','Orlando','FL','344761234','7641238543','sales@sysco.com','ww.sysco.com',NULL),
('General Eletric','100 Happy Trails Dr','Boston','MA','123458743','2134569641','support@ge.com','www.ge.com',NULL),
('Cisco','300 Cisco Dr','Stanford','OR','782315492','7823456723','cisco@cisco.com','www.cisco.com',NULL),
('Goodyear','100 Goodyear Dr','Gary','IN','485321956','5784218427','sales@goodyear.com','www.goodyear.com',NULL),
('Snap-on','42185 Magenta Ave','Lake Falls','ND','387513649','9197345632','support@snapon.com','www.snap-on.com','Good quality tools');

select * from dbo.vendor;
--
insert into dbo.product
(ven_id,pro_name,pro_descript,pro_weight,pro_qoh,pro_cost,pro_price,pro_discount,pro_notes)
values
(1,'hammer','',2.5,45,4.99,7.99,30,'Discounted only when purchased with screwdriver set.'),
(2,'screwdriver','',1.8,120,1.99,3.49,Null,NULL),
(3,'pail','16 Gallon',2.8,48,3.89,7.99,40,NULL),
(4,'cooking oil','peanut oil',15,19,19.99,28.99,NULL,'gallons'),
(5,'frying pan','',3.5,178,8.45,13.99,50,'Currently 1/2 price sale');

select * from dbo.product;
--
insert into dbo.order_line
(ord_id,pro_id,oln_qty,oln_price,oln_notes)
values
(1,2,10,8.0,NULL),
(2,3,7,9.88,NULL),
(3,4,3,6.99,NULL),
(5,1,2,12.76,NULL),
(4,5,13,58.99,NULL);

select * from dbo.order_line;
--
insert into dbo.payment
(inv_id,pay_date,pay_amt,pay_notes)
values
(5,'2008-07-01',5.99,NULL),
(4,'2010-09-28',4.99,NULL),
(1,'2008-07-23',8.75,NULL),
(3,'2010-10-31',19.55,NULL),
(2,'2011-03-29',32.5,NULL);

select * from dbo.payment;
--
insert into dbo.product_hist
(pro_id,pht_date,pht_cost,pht_price,pht_discount,pht_notes)
values
(1,'2005-01-02 11:53:34',4.99,7.99,30,'Discounted when purchased with screwdriver set'),
(2,'2005-02-03 09:13:56',1.99,3.49,NULL,null),
(3,'2005-03-04 23:21:49',3.89,7.99,40,NULL),
(4,'2006-05-06 18:09:04',19.99,28.99,NULL,'gallons'),
(5,'2006-05-07 15:07:29',8.45,13.99,50,'Currently 1/2 price sale');

select * from dbo.product_hist;

insert into dbo.time
(tim_yr,tim_qtr,tim_month,tim_week,tim_day,tim_time,tim_notes)
VALUES
(2008,2,5,19,7,'11:59:59',NULL),
(2010,4,12,49,4,'08:34:21',NULL),
(1999,4,12,52,5,'05:21:34',NULL),
(2011,3,8,36,1,'09:32:18',NULL),
(2001,3,8,36,1,'09:32:21',NULL);
GO

select * from dbo.time;

insert into dbo.sale
(pro_id,str_id,cnt_id,tim_id,sal_qty,sal_price,sal_total,sal_notes)
VALUES
(1,5,1,2,3,9.99,29.4,NULL),
(2,4,3,3,5,4.99,24.99,NULL),
(3,3,5,5,8,9.99,79.99,NULL),
(4,2,2,4,9,19.99,159.99,NULL),
(5,1,4,1,10,9.99,99.99,NULL),
(4,2,1,1,9,49.99,449.99,NULL),
(3,3,3,2,8,9.99,79.99,NULL),
(2,4,5,4,7,4.99,35.99,NULL),
(1,5,2,3,6,1.99,11.99,NULL),
(2,4,4,5,5,10.00,50.00,NULL),
(3,3,5,1,4,5.00,20.00,NULL),
(4,2,1,5,3,8.00,24.00,NULL),
(5,1,4,2,2,5.00,10.00,NULL),
(4,2,2,3,1,100.00,100.00,NULL),
(3,3,5,4,10,10.00,100.00,NULL),
(2,4,2,5,11,12.00,132.00,NULL),
(1,5,5,2,13,13.00,169.00,NULL),
(2,4,3,1,5,5.00,25.00,NULL),
(3,3,5,2,4,6.00,24.00,NULL),
(4,2,4,3,2,8.00,16.00,NULL),
(5,1,1,4,7,7.00,49.00,NULL),
(4,2,5,1,9,6.00,54.00,NULL),
(3,3,2,5,8,8.00,64.00,NULL),
(2,4,4,3,5,5.00,25.00,NULL),
(1,5,3,1,3,23.00,69.00,NULL);
--
insert into dbo.srp_hist
(per_id,sht_type,sht_modified,sht_modifier,sht_date,sht_yr_sales_goal,sht_yr_total_sales,sht_yr_total_comm,sht_notes)
values
(1,'i',getDate(),system_user,getDate(),100000,110000,11000,NULL),
(2,'i',getDate(),system_user,getDate(),150000,175000,17500,NULL),
(3,'u',getDate(),system_user,getDate(),200000,185000,18500,NULL),
(4,'u',getDate(),original_login(),getDate(),210000,220000,22000,NULL),
(5,'i',getDate(),original_login(),getDate(),225000,230000,2300,NULL);

select * from dbo.srp_hist;

-----------
--REPORTS--
-----------

----1----
if object_id(N'dbo.product_days_of_week', N'P') is not NULL
drop proc dbo.product_days_of_week;
GO

create proc dbo.product_days_of_week AS
BEGIN
select pro_name, pro_descript, datename(dw, tim_day-1) 'day_of_week'
from product p
  join sale s on p.pro_id=s.pro_id
  join time t on t.tim_id=s.tim_id
  order by tim_day-1 asc;
END
GO

EXEC dbo.product_days_of_week;


----2----
if object_id(N'dbo.product_drill_down', N'P') is not NULL
drop proc dbo.product_drill_down;
GO

create proc dbo.product_drill_down AS
BEGIN
select pro_name, pro_qoh,
  format(pro_cost, 'C', 'en-us') as cost,
  format(pro_price, 'C', 'en-us') as price,
  str_name,cty_name, ste_name, reg_name
  from product p
    join sale s on p.pro_id=s.pro_id
    join store sr on sr.str_id=s.str_id
    join city c on sr.cty_id=c.cty_id
    join state st on c.ste_id=st.ste_id
    join region r on st.reg_id=r.reg_id
  order by pro_qoh desc;
END
go

EXEC dbo.product_drill_down;


----3----
if object_id(N'dbo.add_payment', N'P') is not NULL
drop proc dbo.add_payment;
GO

create proc dbo.add_payment
  @inv_id_p int,
  @pay_date_p datetime,
  @pay_amt_p decimal(7,2),
  @pay_notes_p varchar(255)
  as
  BEGIN
    insert into payment(inv_id, pay_date, pay_amt, pay_notes)
    VALUES
    (@inv_id_p, @pay_date_p, @pay_amt_p, @pay_notes_p);
  END
  GO

select * from payment;

DECLARE
@inv_id_v int = 5,
@pay_date_v datetime = '2014-01-05 11:56:38',
@pay_amt_v decimal(7,2) = 159.99,
@pay_notes_v varchar(255) = 'testing add_payment';

exec dbo.add_payment @inv_id_v, @pay_date_v, @pay_amt_v, @pay_notes_v;


----4----
if object_id(N'dbo.customer_balance', N'P') is not NULL
drop proc dbo.customer_balance
GO

create proc dbo.customer_balance
@per_lname_p varchar(30)
as 
BEGIN
  select p.per_id, per_fname, per_lname, i.inv_id,
  format(sum(pay_amt), 'C', 'en-us') as total_paid,
  format((inv_total - sum(pay_amt)), 'C', 'en-us') as invoice_diff
  from person p
    join dbo.customer c on p.per_id=c.per_id
    join dbo.contact ct on c.per_id=ct.per_cid
    join dbo.[order] o on ct.cnt_id=o.cnt_id
    join dbo.invoice i on o.ord_id=i.ord_id
    join dbo.payment pt on i.inv_id=pt.inv_id
  where per_lname=@per_lname_p
  group by p.per_id, i.inv_id, per_lname, per_fname, inv_total;
END
GO

declare @per_lname_v varchar(30) = 'smith';

exec dbo.customer_balance @per_lname_v;


----5----
if object_id(N'dbo.store_sales_between_dates', N'P') is not NULL
drop proc dbo.store_sales_between_dates
GO

create proc dbo.store_sales_between_dates
@start_date_p smallint,
@end_date_p SMALLINT
AS
BEGIN
  select st.str_id, format(sum(sal_total), 'C', 'en-us') as 'total sales', tim_yr as year 
  from store st
  join sale s on st.str_id=s.str_id
  join time t on s.tim_id=t.tim_id
  where tim_yr between @start_date_p and @end_date_p
  group by tim_yr, st.str_id
  order by sum(sal_total) desc, tim_yr desc;
end
go

declare
  @start_date_v smallint = 2010,
  @end_date_v smallint = 2013;

exec dbo.store_sales_between_dates @start_date_v, @end_date_v;


----6----
if object_id(N'dbo.trg_check_inv_paid', N'TR') is not null
drop trigger dbo.trg_check_inv_paid
go

create trigger dbo.trg_check_inv_paid
on dbo.payment
after insert as 
begin

update invoice
set inv_paid=0;

update invoice
set inv_paid=1
from invoice as i
  join
  (
    select inv_id, sum(pay_amt) as total_paid
    from payment
    group by inv_id
  ) as v on i.inv_id=v.inv_id
  where total_paid >= inv_total;

end
go

select * from invoice;

select * from payment;

select inv_id, sum(pay_amt) as sum_pmt
from payment
group by inv_id;


----extra credit----
if object_id(N'dbo.order_line_total', N'P') is not null
drop proc dbo.order_line_total
go

create proc dbo.order_line_total as
begin
  select oln_id, p.pro_id, pro_name, pro_descript,
  format(pro_price,'C','en-us') as pro_price,
  oln_qty,
  format((oln_qty * pro_price), 'C', 'en-us') as oln_price,
  format((oln_qty * pro_price) * 1.06,'C','en-us') as total_with_6pct_tax
  from product p
  join order_line ol on p.pro_id=ol.pro_id
  order by p.pro_id;
end
go

exec dbo.order_line_total;
