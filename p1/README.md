# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Project 1 Requirements:

_Parts of the Assignment:_

1. Make diagram for with required tables 
2. Make sure the tables and relationships follow the provided buisness rules
3. Provide required screenshots
4. Link the .mwb and the .sql files

#### Buisness rules:
![Buisness rules](img/br.png)

#### Assignment Screenshots:

### ERD:
![Buisness rules](img/p1erd.png)

###

### Links to files:
[p1.mwb file](docs/p1.mwb)

[p1_solutions.sql file](docs/p1_solutions.sql)