drop schema if exists tdb17c;
create schema if not exists tdb17c;
use tdb17c;

-- PERSON TABLE --

drop table if exists person;
create table if not exists person
(
	per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_ssn BINARY(64) NULL,
    per_pork binary(64) null COMMENT 'only demo purposes do not use salt in the name',
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL,
    per_zip varchar(9) NOT NULL,
    per_email varchar(100) not null,
    per_dob date not null,
    per_type enum('a','c','j') not null,
    per_notes varchar(255) null,
    PRIMARY KEY (per_id),
    UNIQUE INDEX ux_per_ssn (per_ssn ASC)
)
ENGINE = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- attorney table

drop table if exists attorney;
create table if not exists attorney
(
	per_id smallint unsigned not null,
    aty_start_date date not null,
    aty_end_date date null default null,
    aty_hourly_rate decimal(5,2) not null,
    aty_years_in_practice tinyint not null,
    aty_notes varchar(255) null default null,
    primary key (per_id),
    
    INDEX idx_per_id (per_id asc),
    
    constraint fk_attorney_person
		foreign key (per_id)
        references person (per_id)
        on delete no action
        on update cascade
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- client table --

drop table if exists client;
 create table if not exists client
 (
	per_id smallint unsigned not null,
    cli_notes varchar(255) null default null,
    primary key (per_id),
    
	INDEX idx_per_id (per_id asc),
    
    constraint fk_client_person
		foreign key (per_id)
        references person (per_id)
        on delete no action
        on update cascade
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- court --

drop table if exists court;
create table if not exists court
(
	crt_id tinyint unsigned not null auto_increment,
    crt_name varchar(45) not null,
    crt_street varchar(30) not null,
    crt_city varchar(30) not null,
    crt_state char(2) not null,
    crt_zip varchar(9) not null,
    crt_phone bigint not null,
    crt_email varchar(100) null,
    crt_url varchar(100) not null,
    crt_notes varchar(255) null,
    primary key (crt_id)
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- judge table --

drop table if exists judge;
create table if not exists judge
(
	per_id smallint unsigned not null,
    crt_id tinyint unsigned null default null,
    jud_salary decimal(8,2) not null,
    jud_years_in_practice tinyint unsigned not null,
    jud_notes varchar(255) null default null,
    primary key (per_id),
    
    index idx_per_id (per_id asc),
    index idx_crt_id (crt_id asc),
    
    constraint fk_judge_person
		foreign key (per_id)
        references person (per_id)
        on delete no action
        on update cascade,
        
	constraint fk_judge_court
		foreign key (crt_id)
        references court (crt_id)
        on delete no action
        on update cascade
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- judge_hist table --

drop table if exists judge_hist;
create table if not exists judge_hist
(
	jhs_id smallint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    jhs_crt_id tinyint null,
    jhs_date timestamp not null default current_timestamp(),
    jhs_type enum('i','u','d') not null default 'i',
    jhs_salary decimal(8,2) not null,
    jhs_notes varchar(255) null,
    primary key (jhs_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_judge_hist_judge
		foreign key (per_id)
        references judge (per_id)
        on delete no action
        on update cascade
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- case table --

drop table if exists `case`;
create table if not exists `case`
(
	cse_id smallint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    cse_type varchar(45) not null,
    cse_description text not null,
    cse_start_date date not null,
    cse_end_date date null,
    cse_notes varchar(255) null,
    primary key (cse_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_court_case_judge
		foreign key (per_id)
        references judge (per_id)
        on delete no action
        on update cascade
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- bar table --

drop table if exists bar;
create table if not exists bar
(
	bar_id tinyint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    bar_name varchar(45) not null,
    bar_notes varchar(255) null,
    primary key (bar_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_bar_attorney
		foreign key (per_id)
        references attorney (per_id)
        on delete no action
        on update cascade
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

drop table if exists specialty;
create table if not exists specialty
(
spc_id tinyint unsigned not null auto_increment,
per_id smallint unsigned not null,
spc_type varchar(45) not null,
spc_notes varchar(255) null,
primary key (spc_id),

index idx_per_id (per_id asc),

constraint fk_specialty_attorney
	foreign key (per_id)
    references attorney (per_id)
    on delete no action
    on update cascade
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- assignment table --

drop table if exists assignment;
create table if not exists assignment
(
	asn_id smallint unsigned not null auto_increment,
    per_cid smallint unsigned not null,
    per_aid smallint unsigned not null,
    cse_id smallint unsigned not null,
    asn_notes varchar(255) null,
    primary key (asn_id),
    
    index idx_per_cid (per_cid asc),
    index idx_per_aid (per_aid asc),
    index idx_cse_id (cse_id asc),
    
    unique index ux_per_cid_per_aid_cse_id (per_cid asc, per_aid asc, cse_id asc),
    
    constraint fk_assign_case
		foreign key (cse_id)
        references `case` (cse_id)
        on delete no action
        on update cascade,
        
        constraint fk_assignment_client
		foreign key (per_cid)
        references client (per_id)
        on delete no action
        on update cascade,
        
        constraint fk_assignment_attorney
		foreign key (per_aid)
        references attorney (per_id)
        on delete no action
        on update cascade
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- phone table --

drop table if exists phone;
create table if not exists phone
(
	phn_id smallint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    phn_num bigint unsigned not null,
    phn_type ENUM('h','c','w','f') not null comment 'home, sell, work, fax',
    phn_notes varchar(255) null,
    primary key (phn_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_phone_person
		foreign key (per_id)
        references person (per_id)
        on delete no action
        on update cascade
)
engine = InnoDB
default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- data for person table --

start transaction;

insert into person
(per_id, per_ssn, per_pork, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(null,null,null, 'Steve', 'Rogers', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', '1923-10-03', 'c', null),
(null,null,null, 'Bruce', 'Wayne', '1007 Mountian Drive', 'Gotham', 'NY', 003208440, 'bwayne@knology,net', '1968-03-20', 'c', null),
(null,null,null, 'Peter', 'Parker', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', '1988-09-12', 'c', null),
(null,null,null, 'Jane', 'Thompson', '13563 Ocean View Drive', 'Seattle', 'WA', 032084409, 'jthompson@gmail.com', '1978-05-08', 'c', null),
(null,null,null, 'Debra', 'Steele', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verison.net', '1994-09-12', 'c', null),
(null,null,null, 'Tony', 'Stark', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', '1972-05-04', 'a', null),
(null,null,null, 'Hank', 'Pymi', '2355 Brown Street', 'Cleavland', 'OH', 022348890, 'hpym@aol.com', '1980-08-28', 'a', null),
(null,null,null, 'Bob', 'Best', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo,com', '1992-02-10', 'a', null),
(null,null,null, 'Sandra', 'Dole', '87912 Lawernce Ave', 'Atlanta', 'GA', 002348890, 'sdole@gmail.com', '1990-01-26', 'a', null),
(null,null,null, 'Ben', 'Avery', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', '1983-12-24', 'a', null),
(null,null,null, 'Arthur', 'Curry', '3304 Euclid Avenue', 'Miami', 'FL', 000219932, 'acurry@gmail.com', '1975-12-15', 'j', null),
(null,null,null, 'Diana', 'Prince', '944 Green Street', 'Las Vagas', 'NV', 332048823, 'dprince@symaptico.com', '1980-08-22', 'j', null),
(null,null,null, 'Adam', 'Jurris', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', '1995-01-31', 'j', null),
(null,null,null, 'Judy', 'Sleen', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@symaptico,com', '1970-03-22', 'j', null),
(null,null,null, 'Bill', 'Neiderheim', '43567 Neatherlands Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', '1982-03-31', 'j', null);

commit;

-- data for phone --

start transaction;

insert into phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
values
(null, 1, 1232323456,'c', null),
(null, 2, 4345267896,'h', null),
(null, 4, 1223435465,'w', null),
(null, 5, 6576763423,'f', 'fax number ot currently working'),
(null, 6, 9876789098,'c', 'prefers home calls'),
(null, 7, 4567893465,'h', null),
(null, 8, 6567654543,'w', null),
(null, 9, 1243568790,'f', 'work fax number'),
(null, 10, 1234567890,'h', 'prefers cell phone calls'),
(null, 11, 4345567678,'w', 'best number to reach'),
(null, 12, 2343456567,'w', 'call during lunch'),
(null, 13, 1234565456,'h', 'has two office numbers'),
(null, 14, 9142759567,'c', 'prefers cell phone calls'),
(null, 15, 4556676454,'f', 'use for faxing legal docs');

commit;

-- data for client table --

Start transaction;

insert into client
(per_id, cli_notes)
values
(1, null),
(2, null),
(3, null),
(4, null),
(5, null);

commit;

-- data for table bar --

start transaction;

insert into attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
values
(6, '2006-06-12', null, 85, 5, null),
(7, '2003-08-20', null, 130, 28,null),
(8, '2009-12-12', null, 70, 17,null),
(9, '2008-06-08', null, 78, 13,null),
(10, '2011-09-12', null, 60, 24,null);

commit;

-- Data for table --

start transaction;

insert into bar
(bar_id, per_id, bar_name, bar_notes)
values
(null, 6, 'Florida Bar', null),
(null, 7, 'Alabama bar', null),
(null, 8, 'Georgia bar', null),
(null, 9, 'Michigan bar', null),
(null, 10, 'South Carolina bar', null),
(null, 6, 'Montana bar', null),
(null, 7, 'Arizona bar', null),
(null, 8, 'Nevada bar', null),
(null, 9, 'New york bar', null),
(null, 10, 'New York bar', null),
(null, 6, 'Mississippi bar', null),
(null, 7, 'California bar', null),
(null, 8, 'Illinois bar', null),
(null, 9, 'Indiana bar', null),
(null, 10, 'Illinois bar', null),
(null, 6, 'Tallahassee bar', null),
(null, 7, 'Ocala bar', null),
(null, 8, 'Bay County bar', null),
(null, 9, 'Cincinatti bar', null);

commit;

-- Data for table specialty --

start transaction;

insert into specialty
(spc_id, per_id, spc_type, spc_notes)
values
(null, 6, 'buisness', null),
(null, 7, 'traffic', null),
(null, 8, 'bankruptcy', null),
(null, 9, 'insurance', null),
(null, 10, 'judicial', null),
(null, 6, 'enviornment', null),
(null, 7, 'criminal', null),
(null, 8, 'real estate', null),
(null, 9, 'malpractice', null);

commit;

-- Data for court table --

start transaction;

insert into court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
values
(null, 'leon country circit court', '301 south monroe street', 'tallahassee', 'fl', 323035292, 8506065504, 'lccc@us.fl.gov', 'leoncountycircuitcort.gov', null),
(null, 'leon county traffic court', '1921 thomasville road', 'tallahassee', 'fl', 323035292, 8505774100, 'lctc@us.fl.gov', 'leoncountytrafficcourt,gov', null),
(null, 'florida supreme court', '1921 thomasville road', 'tallahassee', 'fl', 323035292, 8504880125, 'fsc@us.fl.gov', 'floridasupremecourt.gov', null),
(null, 'orange county courthouse', '424 north orange avenue', 'orlando', 'fl', 328012248, 2345656565, 'occ@us.fl.gov', 'ninthcuircit.org', null),
(null, 'fifth discruit court of appeal', '300 south beach street', 'daytona beach', 'fl', 321158763, 3434545654, '5dca@us.fl.gov', '5dca.org', null);

commit;

-- data for judge table --

start transaction;

insert into judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
(11, 5, 150000, 10 , null),
(12, 4, 185000, 3 , null),
(13, 4, 135000, 2 , null),
(14, 3, 170000, 6 , null),
(15, 1, 120000, 1 , null);

commit;

-- data for judge_hist --

start transaction;

insert into judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
(null, 11, 3, '2009-01-16', 'i', 130000, null),
(null, 12, 2, '2010-05-27', 'i', 140000, null),
(null, 13, 5, '2000-01-02', 'i', 115000, null),
(null, 13, 4, '2005-07-05', 'i', 135000, null),
(null, 14, 4, '2008-12-09', 'i', 155000, null),
(null, 15, 1, '2011-03-17', 'i', 120000, 'freshman justice'),
(null, 11, 5, '2010-07-05', 'i', 150000, 'assigned to another court'),
(null, 12, 4, '2012-10-08', 'i', 165000, 'became chief justice'),
(null, 14, 3, '2009-04-19', 'i', 170000, 'reassigned to court based upon loacl area population growth');

commit;

-- case table --

start transaction;

insert into `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
values
(null, 13, 'civil', 'client says tha his logo is being used without consent to promote a rival buisness', '2010-09-09', null, 'copyright infringment'),
(null, 12, 'criminal', 'client is charged with assaulting her husband during an argument', '2009-11-18', '2010-12-23', 'assult'),
(null, 14, 'civil', 'client broke an ankle while shopping at a local grocery store. no wet floor sign was posted although the floor had just been mopped', '2008-05-06', '2008-07-23', 'slip and fall'),
(null, 11, 'criminal', 'client was charged with stealing several televisions from his former place of employment. client has a solid alibi', '2011-05-20', null, 'grand theift'),
(null, 13, 'criminal', 'client charged with posession of 10 grams of cocaine, allgegedly found in his glove box by state police', '2011-06-05', null, 'posession of narcotics'),
(null, 14, 'civil', 'client alleges newspape printed false info about his personal ativities while he ran a large laundry buisness in a small nearby town', '2007-01-19', '2007-05-20', 'defamation'),
(null, 12, 'criminal', 'client charged with murder of his co-worker over a lover feud. client has no alibi', '2010-03-20', null, 'murder'),
(null, 15, 'civil', 'client made the horrible mistake of selecting a degree other than IT and had to declare bankrupcy,', '2012-01-26', '2013-02-28', 'bankruptcy');

commit;

-- data for assignment table --

start transaction;

insert into assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
values
(null, 1, 6, 7, null),
(null, 2, 6, 6, null),
(null, 3, 7, 2, null),
(null, 4, 8, 2, null),
(null, 5, 9, 5, null),
(null, 1, 10, 1, null),
(null, 2, 6, 3, null),
(null, 3, 7, 8, null),
(null, 4, 8, 8, null),
(null, 5, 9, 8, null),
(null, 4, 10, 4, null);

commit;

drop procedure if exists CreatePersonSSN;
DELIMITER $$
create procedure CreatePersonSSN()
BEGIN
	declare x,y int;
    set x = 1;
    
    select count(*) into y from person;
    
    while x <= y do
    
		set @pork = random_bytes(64);
        set @ran_num = floor(rand()*(999999999-111111111+1))+111111111;
        set@ssn = unhex(sha2(concat(@pork, @ran_num), 512));
        
        update person
        set per_ssn = @ssn, per_pork = @pork
        where per_id = x;
        
	set x = x +1;
    
    end while;
    
end$$
delimiter ;
call CreatePersonSSN();

show warnings;

select 'show populated per_ssn feilds after calling stored proc' as '';
select per_id, length(per_ssn) from person order by per_id;
do sleep(7);

drop procedure if exists CreatePersonSSN;

select 'can use exit for debugging--like this...:' as '';
-- exit

-- QUESTIONS 1 - 6 AND EXTRA CREDIT --

-- #1 --
drop view if exists v_attorney_info;
create view v_attorney_info as

select
concat(per_lname, ", ", per_fname) as name,
concat(per_street, ", ", per_city, ", ", per_state, ", ", per_zip) as address,
timestampdiff(year, per_dob, now()) as age,
concat('$', format(aty_hourly_rate, 2)) as hourly_rate,
bar_name, spc_type
from person
	natural join attorney
    natural join bar
    natural join specialty
    order by per_lname;

select 'display view v_attorney_info' as '';

select * from v_attorney_info;
drop view if exists v_attorney_info;
do sleep(3);

-- #2 --

select 'Step a) Display all persons DOB months: testing MySQL monthname() function' as '';

select per_id, per_fname, per_lname, per_dob, monthname(per_dob) from person;
do sleep(3);

select 'Step b) Display pertinent judge data' as '';

select p.per_id, per_fname, per_lname, per_dob, per_type 
from person as p 
	natural join judge as j;
do sleep(3);

select 'Final: step c) Stored proc: Display month numbers, month names, and how many judges were born each month' as '';

drop procedure if exists sp_num_judges_born_by_month;
DELIMITER //
create procedure sp_num_judges_born_by_month()
begin
	select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
    from person
		natural join judge
        group by month_name
        order by month;
	end //
    delimiter ;

select 'calling sp_num_judges_born_by_month()' as '';

call sp_num_judges_born_by_month();
do sleep(3);

drop procedure if exists sp_num_judges_born_by_month;
do sleep(3);

-- #3 --

drop procedure if exists sp_cases_and_judges;
delimiter //
create procedure sp_cases_and_judges()
begin

select per_id, cse_id, cse_type, cse_description,
concat(per_fname, " ", per_lname) as name,
concat('(',substring(phn_num, 1, 3), ')', substring(phn_num, 4, 3), '-', substring(phn_num, 7, 4)) as judge_office_num,
phn_type,
jud_years_in_practice,
cse_start_date,
cse_end_date
from person
	natural join judge
    natural join `case`
    natural join phone
where per_type='j'
order by per_lname;

end //
delimiter ;

select 'calling sp_cases_and_judges()' as '';

call sp_cases_and_judges();
drop procedure if exists sp_cases_and_judges;

-- #4 --

select 'show person data *before* adding person record' as '';
select per_id, per_fname, per_lname from person;
do sleep(3);

set @pork=random_bytes(64);
set @num = 000000000;
set @ssn = unhex(sha2(concat(@pork, @num), 512));

insert into person
(per_id, per_ssn, per_pork, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
values
(null, @ssn, @pork, 'Bobby', 'Sue','123 Main st.','Panama City beach','FL', 324530221, 'bsue@gmail.com','192-05-16','j','new district judge');

select 'show person data after adding person record' as '';
select per_id, per_fname, per_lname from person;
do sleep(3);

select'show judge/judge_hist data before AFTER INSERT trigger fires (trg_judge_history_after_insert)' as '';
select * from judge;
select * from judge_hist;
do sleep(5);

drop trigger if exists trg_judge_history_after_insert;

delimiter //
create trigger trg_judge_history_after_insert
after insert on judge
for each row
begin
	insert into judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
	values
    (
    NEW.per_id, NEW.crt_id, current_timestamp(), 'i', NEW.jud_salary,
    concat("modifying user: ", user(), "Notes: ", NEW.jud_notes)
	);
end //
delimiter ;

select 'fire trigger by inserting record into judge table' as '';
do sleep(3);

insert into judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
((select count(per_id) from person), 3, 175000, 31, 'transferred from neighboring juristiction');

select 'show judge/judge_hist data after AFTER INSERT trigger fires (trg_judge_history_after_insert)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

-- 5 --
select 'show judge/judge_hist data before AFTER INSERT trigger fires (trg_judge_history_after_insert)' as '';
select * from judge;
select * from judge_hist;
do sleep (3);

drop trigger if exists try_judge_history_after_update;
delimiter //
create trigger trg_judge_history_after_update
after update on judge
for each row
begin
	insert into judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
    values
    (
    NEW.per_id, NEW.crt_id, current_timestamp(), 'u', NEW.jud_salary,
    concat("modifying user: ", user(), "Notes: ", NEW.jud_notes)
	);
end //
delimiter ;

select 'fire trigger by updating latest judge entry (salary and notes)' as '';

update judge
set jud_salary = 190000, jud_notes = 'senior justice - longest serving member'
where per_id = 16; 

select * from judge;
select * from judge_hist;
do sleep(5);

drop trigger if exists trg_judge_history_after_update;

-- 6 --

drop procedure if exists sp_add_judge_record;
delimiter //

create procedure sp_add_judge_record()
begin
	insert into judge
    (per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
    values
    (6, 1, 110000, 0, concat("New judge was former attorney, ", "Modifying event creator: ", current_user()));
end //

delimiter ;

select '1) check event_scheduler' as '';
show variables like 'event_scheduler';
do sleep(3);

select '2) if not, turn it on...' as '';
set global event_scheduler = ON;

select '3) recheck event_scheduler' as '';
show variables like 'event scheduler';
do sleep(3);

drop event if exists one_time_add_judge;
delimiter //
create event if not exists one_time_add_judge
on schedule
	at now() + interval 5 second
    comment 'adds a judge record only one time'
    do
    begin
    call sp_add_judge_record();
    end//
    
delimiter ;

show events from tdb17c;

-- Extra Credit --
drop event if exists remove_judge_history;
delimiter //
create event if not exists remove_judge_history
on schedule
	every 2 month
    starts now() + interval 3 week
    ends now() + interval 4 year
    comment 'keeps only the first 100 judge records'
    do
    begin
		delete from judge_hist where jhs > 100;
	end //
    
delimiter ;

drop event if exists remove_judge_history;




























































