# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Project 2 Requirements:

_Parts of the Assignment:_

1. Download MongoDB and developer tools
2. Place .exe and .json files in proper location to use
3. Open up the localhost and the client to manipulate data
4. Complete all the questions asked on the assignment details


### Query screenshots:


|        Part 1         |          Part 2          |          Part 3          |
| :------------------------: | :---------------------------------: | :---------------------------------: |
| ![ArrayList](img/p2_1.png) | ![NestedStructures](img/p2_2.png) | ![NestedStructures](img/p2_3.png) |

_Part 4_:

![NestedStructures](img/p2_4.png)

